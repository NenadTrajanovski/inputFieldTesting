package api;

import api.helper.ReadProperties;
import com.microsoft.playwright.APIRequest;
import com.microsoft.playwright.APIRequestContext;
import com.microsoft.playwright.Playwright;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.io.IOException;

import static com.microsoft.playwright.Playwright.create;

public class BaseClass {

    /**
     * Creating instance
     */
    static Playwright pw;
    static APIRequest request;

    public static APIRequestContext createInstance(){
        pw = create();
        request = pw.request();
        return request.newContext();
    }

    //Returning URL and input data
    public static String getPublicURL(String data) throws IOException {
        return ReadProperties.getProperty() + data;
    }


    //Overload of the method getPublicURL using int as a param
    public static String getPublicURL(int data) throws IOException {
        return ReadProperties.getProperty() + data;
    }
}
