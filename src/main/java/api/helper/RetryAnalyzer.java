package api.helper;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RetryAnalyzer implements IRetryAnalyzer{

    /**
     * Utilizing IRetryAnalyzer interface from TestNG
     * The api is failing on every 5th request.
     * The interface is a mechanism to avoid the 500 Server error
     * and to continue running the tests uninterrupted until the bug is fixed
     */

    int counter = 0;
    int retryLimit = 3;

    @Override
    public boolean retry(ITestResult result){
        if (counter < retryLimit){
            counter++;
            return true;
        }
        return false;
    }
}
