package api.helper;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ReadProperties {

    private static final String getOsName = System.getProperty("os.name");
    public static String getProperty()throws IOException {
        Properties prop = new Properties();
        String path = "";
        if (getOsName.equals("Mac OS X"))
            path = System.getProperty("user.dir") + "/src/test/resources/local.properties";
        if (getOsName.startsWith("Windows"))
            path = System.getProperty("user.dir") + "\\src\\test\\resources\\local.properties";
        FileInputStream file = new FileInputStream(path);
        prop.load(file);
        return prop.getProperty("baseURL");
    }
}
