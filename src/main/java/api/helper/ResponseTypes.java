package api.helper;

import com.microsoft.playwright.APIResponse;

public class ResponseTypes {

    /**
     *
     * @param response
     * returning response as text
     */
    public static String returnResponse(APIResponse response){
        return response.text();
    }

    /**
     *
     * @param response
     * returning response status
     */
    public static int returnResponseStatus(APIResponse response){
        return response.status();
    }
}
