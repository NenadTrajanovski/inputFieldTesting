package api.helper;

public class DataInputs {

    //Random string only with letters - return without vowels
    public static final String STRING = "abcd";

    //String with letters without vowels - return the whole string
    public static final String STRING_NO_VOWELS = "zxcvbnm";

    //String with letters only with vowels - return empty string
    public static final String VOWELS_ONLY = "aeou";

    //Empty string - return message
    public static final String EMPTY_STRING = "";

    //Only numbers - return numbers
    public static final int NUMBERS_ONLY = 12345;

    //Numbers as string - return all
    public static final String NUMBERS_AS_STRING = "12345";

    //Only with special chars - return special chars
    public static final String SPEC_CHARS = "~!@$^&&*()-_#%";

    //Spec chars with % -> returns bad request
    public static final String SPEC_CHARS_BAD_REQUEST = "~!@%$^&&*()-_";

    //String with  letters + num - return everything without vowels
    public static final String MIXED_LETTERS_NUMBERS = "asdfnn1234nkasd";

    //String without vowels + numbers - return everything
    public static final String MIXED_WITHOUT_VOWELS = "swfnn1234nkwsd";

    //String with vowels + numbers - return numbers
    public static final String VOWELS_AND_NUMBERS = "a12e09oa31u";

    //String with letters + chars - return everything without vowels
    public static final String LETTERS_SPEC_CHARS = "abc^&_";

    //String without vowels + chars - return everything
    public static final String NO_VOWELS_SPEC_CHARS = "hjkl&^$";

    //String with vowels + spec chars - return spec chars
    public static final String VOWELS_SPEC_CHARS = "a$eo^u*";

    //Letters with vowels + numbers + special characters - return everything without vowels
    public static final String MIX_ALL = "abc^&_123";

    //Letters without vowels + numbers + special chars - return everything
    public static final String MIX_ALL_NO_VOWELS = "wbc@^&_123";
}
