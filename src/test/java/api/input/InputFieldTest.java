package api.input;

import api.BaseClass;
import api.helper.DataInputs;
import api.helper.ResponseTypes;
import api.helper.RetryAnalyzer;
import com.microsoft.playwright.APIResponse;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

import static java.lang.Integer.parseInt;

public class InputFieldTest extends BaseClass {

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void stringOnlyInput() throws IOException {
        APIResponse response = createInstance().get(getPublicURL(DataInputs.STRING));
        Assert.assertEquals(ResponseTypes.returnResponseStatus(response), 200);
        Assert.assertEquals(ResponseTypes.returnResponse(response), "bcd");
    }

    //Input only characters without vowels -> return the whole string
    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void stringNoVowelsInput() throws IOException {
        APIResponse response = createInstance().get(getPublicURL(DataInputs.STRING_NO_VOWELS));
        Assert.assertEquals(ResponseTypes.returnResponse(response), "zxcvbnm");
    }

    //Input with vowels -> return empty string
    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void vowelsOnlyInput() throws IOException {
        APIResponse response = createInstance().get(getPublicURL(DataInputs.VOWELS_ONLY));
        Assert.assertEquals(ResponseTypes.returnResponse(response), "");
    }

    //Input empty string -> return message
    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void emptyStringInput() throws IOException {
        APIResponse response = createInstance().get(getPublicURL(DataInputs.EMPTY_STRING));
        Assert.assertEquals(ResponseTypes.returnResponse(response), "Send GET to /:input");
    }

    //Input numbers -> return numbers
    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void numbersInput() throws IOException {
        APIResponse response = createInstance().get(getPublicURL(DataInputs.NUMBERS_ONLY));
        Assert.assertEquals(parseInt(ResponseTypes.returnResponse(response)), 12345);
    }

    //Input numbers as string -> return all
    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void numbersAsStringInput() throws IOException {
        APIResponse response = createInstance().get(getPublicURL(DataInputs.NUMBERS_AS_STRING));
        Assert.assertEquals(ResponseTypes.returnResponse(response), "12345");
    }

    //Only with special chars - return special chars
    // if # is used the response returned will not contain # and every char that follows
    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void specialCharsInput() throws IOException {
        APIResponse response = createInstance().get(getPublicURL(DataInputs.SPEC_CHARS));
        Assert.assertEquals(ResponseTypes.returnResponse(response), "~!@$^&&*()-_");
    }

    //Using % will return bad request 400
    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void badRequestWithSpecChar() throws IOException {
        APIResponse response = createInstance().get(getPublicURL(DataInputs.SPEC_CHARS_BAD_REQUEST));
        Assert.assertEquals(ResponseTypes.returnResponseStatus(response), 400);
    }

    //Input letters + num -> return everything without vowels
    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void lettersAndNumbersInput() throws IOException {
        APIResponse response = createInstance().get(getPublicURL(DataInputs.MIXED_LETTERS_NUMBERS));
        Assert.assertEquals(ResponseTypes.returnResponse(response), "sdfnn1234nksd");
    }

    //Input without vowels + numbers -> return everything
    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void lettersAndNumbersNoVowelsInput() throws IOException {
        APIResponse response = createInstance().get(getPublicURL(DataInputs.MIXED_WITHOUT_VOWELS));
        Assert.assertEquals(ResponseTypes.returnResponse(response), "swfnn1234nkwsd");
    }

    //Input with vowels + numbers -> return numbers
    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void vowelsAndNumbersInput() throws IOException {
        APIResponse response = createInstance().get(getPublicURL(DataInputs.VOWELS_AND_NUMBERS));
        Assert.assertEquals(parseInt(ResponseTypes.returnResponse(response)), 120931);
    }

    //Input with letters + chars - return everything without vowels
    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void lettersAndSpecCharsInput() throws IOException {
        APIResponse response = createInstance().get(getPublicURL(DataInputs.LETTERS_SPEC_CHARS));
        Assert.assertEquals(ResponseTypes.returnResponse(response), "bc^&_");
    }

    //Input without vowels + chars - return everything
    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void noVowelsSpecCharInput() throws IOException {
        APIResponse response = createInstance().get(getPublicURL(DataInputs.NO_VOWELS_SPEC_CHARS));
        Assert.assertEquals(ResponseTypes.returnResponse(response), "hjkl&^$");
    }

    //Input with vowels + spec chars -> return spec chars
    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void vowelsAndSpecCharsInput() throws IOException {
        APIResponse response = createInstance().get(getPublicURL(DataInputs.VOWELS_SPEC_CHARS));
        Assert.assertEquals(ResponseTypes.returnResponse(response), "$^*");
    }

    //Input with vowels + numbers + special characters -> return everything without vowels
    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void mixedInput() throws IOException {
        APIResponse response = createInstance().get(getPublicURL(DataInputs.MIX_ALL));
        Assert.assertEquals(ResponseTypes.returnResponse(response), "bc^&_123");
    }

    //Input without vowels + numbers + special chars -> return everything
    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void mixedNoVowelsInput() throws IOException {
        APIResponse response = createInstance().get(getPublicURL(DataInputs.MIX_ALL_NO_VOWELS));
        Assert.assertEquals(ResponseTypes.returnResponse(response), "wbc@^&_123");
    }
}
